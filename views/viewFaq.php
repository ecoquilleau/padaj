<div class="container">
    


    <div class="" id="accordion">
        <div class="faqHeader">Questions générales :</div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Is account registration required?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="card-block">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Can I submit my own Bootstrap templates or themes?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">What is the currency used for all transactions?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                </div>
            </div>
        </div>

        <div class="faqHeader">Bailleurs :</div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Who cen sell items?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">I want to sell my items - what are the steps?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">How much do I get from each sale?</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>                    <br />
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Why sell my items here?</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">What are the payment options?</a>
                </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">When do I get paid?</a>
                </h4>
            </div>
            <div id="collapseNine" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                </div>
            </div>
        </div>

        <div class="faqHeader">Locataires :</div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">I want to buy a theme - what are the steps?</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>
                    <br />
                    Once the transaction is complete, you gain full access to the purchased product. 
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h4 class="card-header">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Is this the latest version of an item</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="card-block">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque at unde, autem totam hic asperiores natus dolorum mollitia laudantium rem nesciunt dolore accusantium odit quam officiis amet libero ducimus exercitationem!</p>                </div>
            </div>
        </div>
    </div>
    <p class="text-center"> Si la F.A.Q n'a pas répondu à vos questions, vous pouvez toujours <a href="<?= URL ?>nous-contacter">nous contacter</a>
</div>
    
<style>
    .faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'Glyphicons Halflings';
        content: "e072"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }
</style>