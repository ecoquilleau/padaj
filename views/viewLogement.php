<?php
if(isset($insertion) || isset($suppression)) {   
    if (!isset($_SESSION['refresh'])) {
        $_SESSION['refresh'] = true;
        header('Location:'. URL .'logement/'.$logement[0]->getID());
    }
}
if(isset($_SESSION['refresh'])) {
    unset($_SESSION['refresh']);
}

if(isset($erreur)) { //logement inexistant?>

    <p class="text-center m-3"><?= $erreur?></p><?php

} else { 
        
    if(!isset($_SESSION['ID'])) { //si pas une famille ou un sénior ?>
        <input type="hidden" id="session_id" value="null"/>
        <button type="button" id="showPopUp" class="d-none" data-toggle="modal" data-target="#popUp">Show popup</button>

        <!-- Modal -->
        <div class="modal fade" id="popUp" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Accès interdit</h5>
                    </div>
                    <div class="modal-body">
                        Pour consulter le détail des logements disponibles, vous devez être connecté
                    </div>
                    <div class="modal-footer">
                        <a href="<?= URL ?>" class="btn btn-primary">Retour à l'accueil</a>
                        <a href="<?= URL ?>inscription" class="btn btn-dark">S'inscrire</a>
                        <a href="<?= URL ?>connexion" class="btn btn-dark">Se connecter</a>
                    </div>
                </div>
            </div>
        </div><?php
    }
    
    $qManager = new QuartierManager();
    $quartier = $qManager->getQuartier($logement[0]->getID_QUARTIER());
    $vManager = new VilleManager();
    $ville = $vManager->getVille($quartier[0]->getID_VILLE());

    $ebManager = new Est_bailleurManager();
    $bailleur_id = $ebManager->getBailleurID($logement[0]->getID());
    $cManager = new ClientManager();
    $client = $cManager->getClient($bailleur_id);

    if(isset($_SESSION['ID'])) {
        $fManager = new FavorisManager();
        $isFav = $fManager->estFavoris($_SESSION['ID'], $logement[0]->getID());
    } else {
        $isFav = false;
    }
    ?>

    <div class="bandeau">
        <div class="position-absolute m-4">
            <form action="<?= URL ?>logement/<?= $logement[0]->getID() ?>" method="post">
                <?php if(!$isFav) {?>
                    <button class="fav-button" name="ajout_fav" title="Ajouter aux favoris"><img src="<?= URL ?>images/favoris/non-fav.png" width=70px/></button><?php
                } else {?>
                    <button class="fav-button" name="suppr_fav" title="Supprimer des favoris"><img src="<?= URL ?>images/favoris/fav.png" width=70px/></button><?php
                }?>
            </form>
        </div>
        <img src="https://www.stephanie-dussaillant.fr/wp-content/themes/DWF/img/bandeau-realisations.jpg" height="300px"/>
    </div>
    <div class="d-flex justify-content-around mt-3 infos">
        <div class="prix_service"> 
            Prix de la location : <?= $logement[0]->getPRIX_LOCATION() ?>€<br>
        </div>
        <div class="description">
            <h4>Description : </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla luctus tempor libero, eu dictum sapien commodo posuere. Sed tincidunt a augue vitae blandit. Aenean nec purus vel nisl egestas suscipit... à faire</p>

            <h4>Caractéristique : </h4>
            <strong>Adresse : </strong><?= $logement[0]->getADRESSE() ?><br>
            <strong>Quartier : </strong><?= $quartier[0]->getINTITULE() ?><br>
            <strong>Ville : </strong><?= $ville[0]->getINTITULE() ?><br>
        </div>
        <div class="bailleur">
            <h4>Profil du logeur : </h4>
            <strong>Nom : </strong><?= $client[0]->getNOM() ?><br>
            <strong>Prénom : </strong><?= $client[0]->getPRENOM() ?><br>
            <strong>Date de naissance : </strong><?= implode('/', array_reverse(explode('-', $client[0]->getDATE_DE_NAISSANCE()))) //date format fr ?><br>
            <strong>Mail : </strong><?= $client[0]->getMAIL() ?><br>
            <strong>Téléphone : </strong><?= $client[0]->getTELEPHONE() ?><br>
        </div>
    </div>
    
    <?php

}

?>