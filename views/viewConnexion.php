<?php

if(isset($insertion)) {
    ?><p>Inscription réussie ! Vous pouvez dès à présent vous connecter</p><?php
}

?>

<div class="container connexion">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Se connecter</h5>
                    <?php if(isset($generalError) && $generalError !== "") { ?><p class="text-center comments-error"><?= $generalError ?></p><?php } else { ?><p class="invisible">  </p><?php } ?>
                    <form class="form-signin" action="<?= URL ?>connexion" method="post">
                        <div class="form-label-group">
                            <input type="email" class="form-control"  name="mail" id="mail" placeholder="Adresse mail" required autofocus>
                            <label for="inputEmail">Adresse mail</label>
                        </div>
                        <div class="form-label-group">
                            <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Mot de passe" required>
                            <label for="inputPassword">Mot de passe</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Se souvenir de moi</label>
                        </div>
                        <button class="btn btn-dark btn-block text-uppercase" type="submit" name="signin" id="signin">Se connecter</button>
                    </form>
                    <p class="text-center mt-3">Je n'ai pas encore de compte : <a class="link-white" href="<?= URL ?>inscription">s'inscrire</a></p>
                </div>
            </div>
        </div>
    </div>
</div>