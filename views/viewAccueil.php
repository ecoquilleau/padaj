<?php 
    $vManager = new VilleManager();
    $villes = $vManager->getVilles();
?>

<!-- Trouver/Proposer mon logement -->
<div class="bannière-site">
    <div class="bannière-left">
        <div id="form-tl" class="form-accueil">
            <!-- Trouver un logement form -->
            <div class="form-accueil-content">
                <a id="close-tl" class="close-form"><img src="<?= URL ?>images/close.png" width=40px/></a>
                <h2 class="title-accueil-form">Où souhaites tu habiter ?</h2>
                <form action="<?= URL ?>trouver-logement" method="post">
                    <div class="d-flex justify-content-center">
                        <select class="form-control w-75 select-ville" name="ville" id="ville_tl" required><?php
                            foreach($villes as $ville) {
                                ?><option value="<?= $ville->getID() ?>"><?= $ville->getINTITULE() ?> (<?= $ville->getCODE_POSTAL() ?>)</option><?php
                            }?>
                        </select>
                    </div>
                   <!-- <p class="text-center color-white mt-4 mb-0">Quel est ton budget mensuel ? ( € )</p>
                    <div class="d-flex justify-content-center">
                        <input class="form-control w-75" type="number" min=0 name="budget" id="budget" placeholder="150" required/>
                    </div>
                    <p class="text-center color-white mt-4 mb-0">Es-tu prêt à faire des services ?</p>
                    <div class="d-flex justify-content-center">
                        <div class=" custom-control custom-checkbox mr-4">
                            <input type="checkbox" class="custom-control-input one-check" name="service" id="service_1" value="1" required>
                            <label class="custom-control-label mt-0" for="service_1">OUI</label>
                        </div>
                        <div class="custom-control custom-checkbox ml-4">
                            <input type="checkbox" class="custom-control-input one-check" name="service" id="service_0" value="0" required>
                            <label class="custom-control-label mt-0" for="service_0">NON</label>
                        </div>
                    </div> -->
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-dark mt-4" name="trouver_logement">Lancer la recherche</button>
                    </div>
                </form>
            </div>
        </div>
        <a id="button-tl" class="btn" role="button">Trouver mon logement</a>
    </div>
    <div class="bannière-right">
        <a id="button-pl" class="btn" role="button">Proposer mon logement</a>
        <div id="form-pl" class="form-accueil">
            <!-- Proposer un logement form -->
            <div class="form-accueil-content">
                <a id="close-pl" class="close-form"><img src="<?= URL ?>images/close.png" width=40px/></a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="how-it-works">
        <h3 class="text-center mt-4 color-green">Comment ça marche ?</h3>
            <div class="row text-center mt-4">
                <div class="col-6 col-sm-4 col-lg-4 col-xl-2">
                    <div class="bloc-green mt-3" >On a pas les icônes </div>
                        <span class="">Recherche les logements disponibles dans la ville de ton choix</span>
                </div>
                <div class="col-6 col-sm-4 col-lg-4 col-xl-2">
                    <div class="bloc-green mt-3">On a pas les icônes</div>
                        <span class="">Trouve ton logement idéal et contacte directement ton futur colocataire  </span>
                    </div>
                <div class="col-6 col-sm-4 col-lg-4 col-xl-2">
                    <div class="bloc-green mt-3">On a pas les icônes</div>
                        <span class="">Discute et fais connaissance avec ton futur colocataire</span>
                </div>
                <div class="col-6 col-sm-4 col-lg-4 col-xl-2">
                    <div class="bloc-green mt-3">On a pas les icônes</div>
                        <span class="">Définissez ensemble les conditions de votre future cohabitation </span>
                </div>
            <div class="col-6 col-sm-4 col-lg-4 col-xl-2">
                <div class="bloc-green mt-3">On a pas les icônes</div>
                    <span class="" > Signe ton contrat de bail directement en ligne et télécharge tes documents  </span>
            </div>
            <div class="col-6 col-sm-4 col-lg-4 col-xl-2">
                <div class="bloc-green mt-3">On a pas les icônes</div>
                    <span class="color-green"></span>
            </div>
        </div>
    </div>
</div>
    
<div class="container-fluid">
    <div class="with-padaj mt-5">
        <div class="row">
            <div class="with-padaj-left text-center col-xl-6">
                <h3 class="color-green text-center mb-5">Avec P A D A J ...</h3>
                <div class="row mb-5">
                    <div class="row mb-5">
                        <div class="col-xl-2">
                            <div class="bloc-green">On a pas les icônes</div>
                        </div>
                        <div class="col-xl-4">
                            <p>Accompagnement, PADAJ est présent t’accompagne dans ta recherche de logement et t’accompagne à chaque étape et reste disponible pour répondre à tes questions 7/7 et 24/24.</p>
                        </div>
                        <div class="col-xl-2">
                            <div class="bloc-green">On a pas les icônes</div>
                        </div>
                        <div class="col-xl-4">
                            <p>Sécurité, toutes les annonces disponibles sur PADAJ sont vérifiées ainsi que les hébergeant. </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-2">
                            <div class="bloc-green">On a pas les icônes</div>
                        </div>
                        <div class="col-xl-4">
                            <p>Simplicité, avec PADAJ toutes les formalités de la location sont simples : signe tes contrats en ligne et paie directement tes loyers par carte bancaire, prélèvement, lydia ou encore Paypal.</p>
                        </div>    
                        <div class="col-xl-2">
                            <div class="bloc-green">On a pas les icônes</div>
                        </div>
                        <div class="col-xl-4">
                            <p>Assurances, avec PADAJ et Axa ta cohabitation intergénérationnelle est assurée contre tous les imprévus. Tu démarre une nouvelle aventure l’esprit tranquille. </p>
                        </div>
                    </div>
                </div>
            </div>
                        <div class="with-padaj-left text-center col-xl-6">
                            <h3 class="color-green text-center mb-5">Les témoignages</h3>
                        </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="garanties mt-5">
            <h3 class="text-center color-green mb-5">Découvrez nos garanties</h3>
                <div class="row text-center">
                    <div class="col-xl-6">
                        <h3 class="color-green">Hébergé</h3>
                            <div class="bandeau-grey">On a pas le contenu</div>
                        </div>
                            <div class="col-xl-6">
                                <h3 class="color-green">Hébergeant</h3>
                            <div class="bandeau-grey">On a pas le contenu</div>
                        </div>
                    </div>
                </div>
            </div>
        

        <div class="container">
        <div class="garanties mt-5">
            <h3 class="text-center color-green mb-5">En savoir plus...</h3>
                <div class="row text-center">
                    <div class="col-xl-5">
                        <h3 class="color-green">Nos partenaires</h3>
                            <div class="bandeau-grey">On a pas le contenu</div>
                        </div>
                        <div class="col-xl-2"></div>
                            <div class="col-xl-5">
                                <h3 class="color-green">Le projet PADAJ</h3>
                            <div class="bandeau-grey">On a pas le contenu</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>











</div>