<?php 
    if (!isset($_SESSION['refresh'])) {
        $_SESSION['refresh'] = true;
        header('Location:'. URL .'trouver-logement');
    } else {
        unset ($_SESSION["refresh"]);
    }

    $vManager = new VilleManager();
    $qManager = new QuartierManager();
    $psManager = new Possede_serviceManager;
    $sManager = new ServiceManager();

    if(!empty($_SESSION['logements'])) {?>
        <div class="container">
        <h3 class="title-account">Logements trouvés</h3>
            <div class="d-flex justify-content-around flex-wrap"><?php              
                foreach($_SESSION['logements'] as $logement) {
                    $ville_logement = $vManager->getVilleOfQuartier($logement->getID_QUARTIER());
                    $quartier_logement = $qManager->getQuartier($logement->getID_QUARTIER());
                    $services_id = $psManager->getServicesOfLogement($logement->getID());
                    $services_logement = $sManager->getServicesPrecis($services_id);
                    ?>
                    
                    <a class="article-link" href="<?= URL ?>logement/<?= $logement->getID() ?>">
                    <div class="article-logement pt-3 row">
                    <div class="col-xl-5">
                        <img src="<?= URL ?>images/cobaye.jpg" width="150px">
                        </div>
                        <div class="col-xl-7">
                        <p class=""><span class="size1 "> <?= $ville_logement[0]->getINTITULE() ?></p>
                        <p> </span> à partir de <span class="color-green size1 text-right"><?= $logement->getPRIX_LOCATION() ?>€/mois </span></p>
                        
                        <div class="service-effectuables"> <p class=" services">Services effectuables : <?= sizeof($services_logement)?></p>
                        </div>
                        </div>
                        </div>
                        </a>
                    <?php
                }?>
            </div>
        </div><?php
    } else {?>
        <h3 class="text-center">Oups ! Aucun résultat !</h3><?php
    }