<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-padaj static-top header-css shadow">
  <div class="container">
    <a class="logo-hover navbar-brand align-middle" href="<?= URL ?>">
          <img src="<?= URL ?>images/logo.png" width=72px alt="Logo PADAJ"><span class="font-weight-bold header-green">P A D A J</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
      <?php if(isset($_SESSION['ID'])) { ?>
        <ul class="navbar-nav ml-auto no-hamburger">
          <li class="nav-item">
              <a class="nav-link header-green" href="<?= URL ?>messagerie"><img src="<?= URL ?>images/header/message.png" width=35px/></a>
          </li>
          <li class="nav-item">
              <a class="nav-link header-green" href="<?= URL ?>favoris"><img src="<?= URL ?>images/header/favoris.png" width=35px/></a>
          </li>
          <li class="nav-item dropdown">
        <a class="nav-link header-green" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="<?= URL ?>images/header/compte.png" width=35px/>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <p class="text-dropdown">Mon compte</p>
          <a class="dropdown-item header-green" href="#">Mon profil</a>
          <a class="dropdown-item header-green" href="#">Mes annonces</a>
          <a class="dropdown-item header-green" href="#">Mes documents</a>
          <form class="form-deco" action="<?= URL ?>connexion" method="post" id="deconnexion_form">
            <input type="submit" class="btn btn-dark btn-dropdown" name="deconnexion" value="Se déconnecter">
          </form>
        
        </div>
        </ul>
        <!-- contenu du menu hamburger -->
        <ul class="navbar-nav ml-auto hamburger">
          <li class="nav-item">
              <a class="nav-link header-green nav-item-img" href="<?= URL ?>messagerie"><img src="<?= URL ?>images/header/message.png" width=35px/><span class="nav-item-text">Messagerie</span></a>
          </li>
          <li class="nav-item">
              <a class="nav-link header-green nav-item-img" href="<?= URL ?>favoris"><img src="<?= URL ?>images/header/favoris.png" width=35px/><span class="nav-item-text">Mes logements favoris</span></a>
          </li>
          <li class="nav-item">
              <p class="nav-not-link header-green nav-item-img"><img src="<?= URL ?>images/header/compte.png" width=35px/><span class="nav-item-text">Mon compte</span></p>
              <a class="nav-link header-green nav-item-img" href="<?= URL ?>#">Mon profil</a>
              <a class="nav-link header-green nav-item-img" href="<?= URL ?>#">Mes annonces</a>
              <a class="nav-link header-green nav-item-img" href="<?= URL ?>#">Mes documents</a>
              <form class="form-deco" action="<?= URL ?>connexion" method="post" id="deconnexion_form">
                <input type="submit" class="btn btn-dark btn-hamburger" name="deconnexion" value="Se déconnecter">
              </form>
          </li>
        </ul>
        <?php
      } else {?>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link header-green" href="<?= URL ?>connexion">Se connecter</a>
          </li>
          <li class="nav-item">
            <a class="nav-link header-green" href="<?= URL ?>inscription">S'inscrire</a>
          </li>
        </ul><?php
      }?>
    </div>
    </div>
  </div>
</nav>
