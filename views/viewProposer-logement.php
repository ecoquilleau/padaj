<?php 
if(isset($_SESSION['id_ville'])) { //reponse ajax - lors de la réactualisation
    $_SESSION['save_id_ville'] = $_SESSION['id_ville'];
    unset($_SESSION['id_ville']);
    $qManager = new QuartierManager();
    $_SESSION['quartiers'] = $qManager->getQuartiersOfVille($_SESSION['save_id_ville']);
}

if(isset($_POST['id_ville'])) { //reponse ajax - selection de la ville
    $_SESSION['id_ville'] = $_POST['id_ville'];
}

if(!isset($_SESSION['ID'])) { //si pas connecté
    $_SESSION['PL'] = true;
    header('Location: '. URL .'inscription');
}

if (!isset($_SESSION['refresh'])) {
    $_SESSION['refresh'] = true;
    header('Location:'. URL .'proposer-logement/'.$_SESSION['etape_pl']);
} else {
    unset ($_SESSION["refresh"]);
}

if(isset($_SESSION['ID_STATUT']) && $_SESSION['ID_STATUT'] === "1") { //si pas une famille ou un sénior ?>
    <input type="hidden" id="id_statut" value="<?= $_SESSION['ID_STATUT'] ?>"/>
    <button type="button" id="showPopUp" class="d-none" data-toggle="modal" data-target="#popUp">Show popup</button>

    <!-- Modal -->
    <div class="modal fade" id="popUp" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Accès interdit</h5>
                </div>
                <div class="modal-body">
                    Les étudiants ne peuvent pas proposer de logement.<br>
                    Seuls les familles et les séniors sont autorisés à le faire.
                </div>
                <div class="modal-footer">
                    <a href="<?= URL ?>" class="btn btn-primary">Retour à l'accueil</a>
                </div>
            </div>
        </div>
    </div><?php
}

$vManager = new VilleManager();
$villes = $vManager->getVilles();

$sManager = new ServiceManager();
$services = $sManager->getServices();

?>

<?php
if($_SESSION['etape_pl'] === "2") {?>

    <h3 class="font-weight-bold text-center mt-3 mb-3 margin-t">Étape 2 : Décrivez votre logement</h3>
            
    <form action="<?= URL ?>proposer-logement/3" method="post">

        <div class="w-50 m-auto">
            <p class="text-center">Surface de votre logement ( m² )</p>
            <input class="form-control" type="number" min=0 name="surface_totale" id="surface_totale" <?php if(isset($_SESSION['surface_totale'])) {?> value="<?= $_SESSION['surface_totale'] ?>" <?php } ?> required/><br>
            <p class="text-center">Surface de la chambre à disposition ( m² )</p>
            <input class="form-control" type="number" name="surface_chambre" id="surface_chambre" <?php if(isset($_SESSION['surface_chambre'])) {?> value="<?= $_SESSION['surface_chambre'] ?>" <?php } ?> required/><br>
            <p class="text-center">Durée souhaitée de la location ( en mois )</p>
            <input class="form-control" type="number" name="duree" id="duree" <?php if(isset($_SESSION['duree'])) {?> value="<?= $_SESSION['duree'] ?>" <?php } ?> required/><br>
            <p class="text-center">Services souhaités</p><?php
            $servicesChecked = array();
            if(isset($_SESSION['servicesList'])) {
                $servicesChecked = explode(',', $_SESSION['servicesList']);
            }
            foreach($services as $service) {
                $inputChecked = false;
                foreach($servicesChecked as $serviceChecked) {
                    if($serviceChecked === $service->getID()) {
                        $inputChecked = true;
                    }
                }?>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="service[]" id="<?= $service->getID() ?>" value="<?= $service->getID() ?>" <?php if($inputChecked) { ?> checked <?php } ?>>
                    <label class="custom-control-label" for="<?= $service->getID() ?>"><?= $service->getINTITULE() ?> (<?= $service->getFREQUENCE() ?> fois / mois) : - <?= $service->getREMISE() ?>€</label>
                </div><?php
            }?>
        </div>
        <a class="btn btn-dark btn-logement" href="<?= URL ?>proposer-logement/1">Etape précédente</a>
        <button type="submit" class="btn btn-dark float-right mr-5" name="etape_description" id="etape_description">Etape suivante</button>

    </form><?php

} else if($_SESSION['etape_pl'] === "3") {?>
    
    <div class="d-none">
        <form action="<?= URL ?>proposer-logement/4" method="post">
            <button type="submit" class="btn btn-dark float-right mr-5" name="etape_estimation" id="etape_estimation">Etape suivante</button>
        </form>
    </div>

    <h3 class="font-weight-bold text-center mt-3 mb-3 margin-t">Étape 3 : Estimation de votre logement</h3>

    <div id="loader" class="d-flex justify-content-center">
        <img src="<?= URL ?>images/loader.gif" width=400px alt=”loader” >
    </div><?php

} else if($_SESSION['etape_pl'] === "4") {?>

    <h3 class="font-weight-bold text-center mt-3 mb-3 margin-t">Étape 4 : Résultat</h3>

    <?php if($_SESSION['insertionsValides']) {?>
        <p class="text-center">Votre location a bien été enregistré.</p>
        <p class="text-center">Le prix de location de votre logement dans le quartier "<?= $_SESSION['quartier'][0]->getINTITULE() ?>" a été estimé à <?= $_SESSION['prix_location'] ?>€</p><?php
    } else {?>
        <p class="text-center">Un problème est survenu.</p><?php
    }?>

    <div class="text-center"><a class="btn btn-dark" href="<?= URL ?>">Retour à l'accueil</a></div>
    <?php
    
} else {?>
    <h3 class="font-weight-bold text-center mt-3 mb-3 margin-t">Étape 1 : Où se trouve votre logement ?</h3>
        
    <form action="<?= URL ?>proposer-logement/2" method="post">
        <div class="w-50 m-auto">
            <p class="text-center">Ville</p>
            <select class="form-control select-ville" name="ville" id="ville_pl" required>
                <option value="" selected disabled hidden>Choisissez une ville</option><?php
                foreach($villes as $ville) {
                    //garder la ville sectionnée s'il y en a une
                    if(isset($_SESSION['save_id_ville']) && $_SESSION['save_id_ville'] === $ville->getID()) {
                        ?><option value="<?= $ville->getID() ?>" selected=selected><?= $ville->getINTITULE() ?> (<?= $ville->getCODE_POSTAL() ?>)</option><?php
                    } else {
                        ?><option value="<?= $ville->getID() ?>"><?= $ville->getINTITULE() ?> (<?= $ville->getCODE_POSTAL() ?>)</option><?php
                    }
                }?>
            </select><br><?php
            if(isset($_SESSION['save_id_ville'])) {?>
                <p class="text-center">Quartier</p>
                <select class="form-control select-ville" name="quartier" id="quartier_pl" required>
                    <option value="" selected disabled hidden>Choisissez un quartier</option><?php
                    foreach($_SESSION['quartiers'] as $quartier) {
                        if(isset($_SESSION['id_quartier']) && $_SESSION['id_quartier'] === $quartier->getID()) {
                            ?><option value="<?= $ville->getID() ?>" selected=selected><?= $quartier->getINTITULE() ?></option><?php
                        } else {
                            ?><option value="<?= $quartier->getID() ?>"><?= $quartier->getINTITULE() ?></option><?php
                        }
                    }?>
                </select><br>
                <p class="text-center">Adresse</p>
                <input class="form-control" type="text" name="adresse" id="adresse" <?php if(isset($_SESSION['adresse'])) {?> value="<?= $_SESSION['adresse'] ?>" <?php } ?> required/><br><?php
            }?>
        </div>
        <button type="submit" class="btn btn-dark float-right mr-5" name="etape_localisation" id="etape_localisation">Etape suivante</button>
    </form><?php
}