<?php
$lManager = new LogementManager();
$qManager = new QuartierManager();
$vManager = new VilleManager();
?>

<h3 class="title-account">Mes logements favoris</h3>
<div class="container">
<div class="d-flex justify-content-around flex-wrap">
<?php
if($favoris != null) {
    foreach($favoris as $favori) {
        $logement_details = $lManager->getLogement($favori->getID_LOGEMENT());
        $quartier = $qManager->getQuartier($logement_details[0]->getID_QUARTIER());
        $ville = $vManager->getVille($quartier[0]->getID_VILLE());
        ?>
        <a class="article-link" href="<?= URL ?>logement/<?= $logement_details[0]->getID() ?>">
        <div class=" p-2">
        <img src="<?= URL ?>images/cobaye.jpg" width=300px>
        <p class="text-center size">Ville : <span class=""><?=$ville[0]->getINTITULE() ?></span>
        <div class="">
        <p class="m-0"><span class=" "><?= "Prix "?> €/mois</span></p>
        </div>
            <div class=""> 
                <p class="text-center services">Services effectuables : <?= "nombre services"?></p>            
            </div>
        </div>
        </a>
        <?php
    }
} else {
    ?><p>Aucun favori pour le moment.<br>Pour ajouter un logement en favori, cliquez sur le coeur situé en haut à gauche de la page du logement souhaité.</p><?php
}?>

</div>
</div>
</div>