
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-xl-3">
        <div class="col-12 col-md-6 col-sm-12 col-xl-12 text-center">
          <h3 class="mb-4  text-uppercase">P A D A J</h3>
            <ul class="list-group">
            <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" target=""> Mentions légales</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" target=""> CGU</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" target="">Protection des données</a>       
              </li class="list-group-item bg-transparent border-0 p-0 mb-2">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" target="">Frais de service</a>       
              </li class="list-group-item bg-transparent border-0 p-0 mb-2">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" target="">Où sommes-nous</a>       
              </li class="list-group-item bg-transparent border-0 p-0 mb-2">
            </ul>
        </div>
        </div>

        <div class=" col-md-6 col-xl-3 text-center">
          <div class="col-12 col-md-6 col-sm-12 col-xl-12">
            <h3 class="mb-4  text-uppercase">Hébergé</h3>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" target=""> Comment ça marche ?</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" target=""> Assurances</a>
              </li>
            </ul>
          </div>
        </div>

<div class="col-md-6 col-xl-3 footer-part1">
        <div class="col-12 col-md-6 col-sm-12 col-xl-12 text-center">
          <h3 class="mb-4  text-uppercase">Hébergeant</h3>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" > Comment ça marche ?</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="#" >Assurances</a>       
              </li class="list-group-item bg-transparent border-0 p-0 mb-2">
            </ul>
  </div>
</div>

<div class="col-md-6 col-xl-3 footer-part1 color">
        <div class="col-12 col-md-6 col-sm-12 col-xl-12 text-center">
          <h3 class="mb-4  text-uppercase">Assistance</h3>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
              <p>06060606</p>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <p>contact@padaj.eu</p> 
              </li class="list-group-item bg-transparent border-0 p-0 mb-2">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="<?= URL ?>nous-contacter">Nous contacter</a> 
              </li class="list-group-item bg-transparent border-0 p-0 mb-2">
            </ul>
      
        </div>
</div>

<div class="separator-green col-xl-12 mb-4"></div>
<div class="col-md-6 col-xl-4 footer-part1 color ">
        <div class="col-12 col-md-6 col-sm-12 col-xl-12 text-center">
          
          <h3>Suivez-nous !</h3>
                <img src="images/reseaux/facebook.png">
                  <img src="images/reseaux/instagram.png " width="68px">
                <img src="images/reseaux/twitter.png">
              <img src="images/reseaux/linkedin.png">
        </div>
</div>
<div class="col-md-6 col-xl-4 footer-part1 color text-center">
        <div class="col-12 col-md-6 col-sm-12 col-xl-12">
          <h3>Mode de paiement</h3>
        </div>
</div>
<div class="col-md-6 col-xl-4 footer-part1 color">
        <div class="col-12 col-md-6 col-sm-12 col-xl-12 text-center">
          <img src="images/logo.png" width="100px">
          <p class="color-green">© 2020 PADAJ</p>
        </div>
</div>



</div>
</div>

</footer>
