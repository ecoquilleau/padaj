<div class="register">
    <div class="row">
        <div class="col-md-3 col-xl-4 register-left">
            <img src="<?= URL ?>images/logo-v2.png" alt=""/>
            <h3>Bienvenue</h3>
            <h5>Inscrivez-vous dès maintenant et profitez des services de PADAJ</h5>
            <p>Je possède déjà un compte</p>
            <a class="btn-connexion" href="<?= URL ?>connexion">Se connecter</a><br/>
        </div>
        <div class="col-md-9 col-xl-8 register-right">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="register-heading">Inscrivez-vous !</h3>
                    <?php if($generalError !== "") { ?><p class="text-center comments-error"><?= $generalError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                    <form class="row register-form" action="<?= URL ?>inscription" method="post">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nom *" value="<?= $name ?>" required/>
                                <?php if($nameError !== "") { ?><p class="comments-error"><?= $nameError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Prénom *" value="<?= $firstname ?>" required/>
                                <?php if($firstnameError !== "") { ?><p class="comments-error"><?= $firstnameError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="birthday" id="birthday" placeholder="Date de naissance *" value="<?= $birthday ?>" onfocus="(this.type='date')" onblur="(this.type='text')" required/>
                                <?php if($birthdayError !== "") { ?><p class="comments-error"><?= $birthdayError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="status" id="status" required>
                                    <option value="" <?php if($status === "") {?> selected <?php } ?> disabled hidden>Quel est votre statut ? *</option>
                                    <option value="1" <?php if($status === "1") {?> selected <?php } ?>>Etudiant</option>
                                    <option value="2" <?php if($status === "2") {?> selected <?php } ?>>Famille</option>
                                    <option value="3" <?php if($status === "3") {?> selected <?php } ?>>Senior</option>
                                </select>
                                <p class="invisible"> invisible </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control" name="mail" id="mail" placeholder="Mail *" value="<?= $mail ?>" required/>
                                <?php if($mailError !== "") { ?><p class="comments-error"><?= $mailError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                            </div>
                            <div class="form-group">
                                <input type="text" minlength="10" maxlength="10" name="phone" id="phone" class="form-control" placeholder="Téléphone" value="<?= $phone ?>"/>
                                <?php if($phoneError !== "") { ?><p class="comments-error"><?= $phoneError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Mot de passe *" required/>
                                <?php if($pwdError !== "") { ?><p class="comments-error"><?= $pwdError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control"  name="pwd_conf" id="pwd_conf" placeholder="Confirmation du mot de passe *" required/>
                                <?php if($pwdError !== "") { ?><p class="comments-error"><?= $pwdError ?></p><?php } else { ?><p class="invisible"> invisible </p><?php } ?>
                            </div>
                            
                            <input type="submit" class="btn-inscription" name="register" id="register" value="S'inscrire"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>