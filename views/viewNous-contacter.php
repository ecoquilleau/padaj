
<div class="container">
    <div class="col-xl-12">
        <div class="row">
            <div class="col-xl-12 page-header text-center">
            <div class="border-form"> <h1>Nous contacter</h1> </div>
            </div>
        </div>
        <form id="contact-form" method="POST" action="<?php echo URL ?>nous-contacter" role="form">
            <div class="row">
            <div class="col-xl-5 formpart1">
                <div class="col-md-12 col-xl-12 form-margin">
                    
                    <input type="text" id="firstname" name="firstname" required  class="" placeholder="Prénom *" value="<?php echo $firstname;?>">
                    <p class="comments-error"><?php echo $firstnameError; ?></p>
                </div>
                <div class="col-md-12 col-xl-12 form-margin">
                    
                    <input type="text" id="name" name="name" required class="" placeholder="Nom *" value="<?php echo $name; ?>">
                    <p class="comments-error"><?php echo $nameError; ?></p>
                </div>
                <div class="col-md-12 col-xl-12 form-margin">
                    <input type="email" id="email" name="email" required class="" placeholder="Email *" value="<?php echo $email; ?>">
                    <p class="comments-error"><?php echo $emailError; ?></p>
                </div>
                <div class="col-md-12 col-xl-12 form-margin">
                    
                    <input type="tel" id="phone" name="phone" class="" placeholder="Téléphone" value="<?php echo $phone; ?>">
                    <p class="comments-error"><?php echo $phoneError; ?></p>
                </div>
                <div class="thanks-form">  
        <p style="display:<?php if($isSuccess) echo "block"; else echo "none";?>"> Votre message a bien été envoyé !</p> 
        </div>

                                
                </div>
                <div class="col-xl-7 formpart2">
                    <textarea id="message" name="message" class="col-xl-12" placeholder="Message *" rows="9"  required><?php echo $message; ?></textarea>
                    <p class="comments-error"><?php echo $messageError; ?></p>
                    <div class="col-md-12">
                        <p class="form-required"> <span class="font-weight-bold">*</span> Ces informations sont requises</p>
                    </div>
    
                    <div class="col-md-12">
                        <input type="submit" class="btn-form" value="Envoyer" name="send">
                    </div>
                </div>
                
                
                
    
                    

                </div> 


        </div>    
        </form>
    </div>
</div>