<?php
include(dirname(__DIR__).'\padaj\models\Logement.php');
include(dirname(__DIR__).'\padaj\models\Quartier.php');
/* include pour le site en ligne
include(dirname(__DIR__).'/www/models/Quartier.php');
include(dirname(__DIR__).'/www/models/Logement.php');
*/
session_start();
define('URL', str_replace("index.php", "", (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]"));

require_once('controllers/Router.php');

$router = new Router();
$router->routerReq();