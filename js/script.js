jQuery(document).ready(function($) {

    //connaitre la page courante
    var url = window.location.href;
    var tabUrl = url.split('/');
    var pageName = tabUrl[tabUrl.length - 1];

    //hover des boutons Trouver & Proposer un logement
    $('.bannière-left > a').hover(function() {
        $('.bannière-left').addClass("buttonIsHover");
    }, function(){
        $('.bannière-left').removeClass("buttonIsHover");
    });
    $('.bannière-right > a').hover(function() {
        $('.bannière-right').addClass("buttonIsHover");
    }, function(){
        $('.bannière-right').removeClass("buttonIsHover");
    });

    /******* Animation des formulaires d'accueil ****/
    //clique sur le bouton "Trouver mon logement"
    $('#button-tl').on('click', function() {
        $('#button-tl').css('display', 'none');
        $('.bannière-left').css('justify-content', 'flex-end');
        $('#form-tl.form-accueil').animate({width: '100%'}, function() {
            $('#form-tl > .form-accueil-content').css('display', 'block');
        });
    });

    $('#close-tl').on('click', function() {
        $('#form-tl.form-accueil').animate({width: '0%'}, function() {
            $('#button-tl').css('display', 'block');
            $('.bannière-left').css('justify-content', '');
        });
        $('#form-tl > .form-accueil-content').css('display', 'none');
    });

    //clique sur le bouton "Proposer mon logement"
    $('#button-pl').on('click', function() {
        $('#button-pl').css('display', 'none');
        $('.bannière-right').css('justify-content', 'flex-start');
        $('#form-pl.form-accueil').animate({width: '100%'}, function() {
            $('#form-pl > .form-accueil-content').css('display', 'block');
        });
    });

    $('#close-pl').on('click', function() {
        $('#form-pl.form-accueil').animate({width: '0%'}, function() {
            $('#button-pl').css('display', 'block');
            $('.bannière-right').css('justify-content', '');
        });
        $('#form-pl > .form-accueil-content').css('display', 'none');
    });

    /* Une checkbox à cocher --> accepter des services ? */
    $('.one-check').click(function() {
        $('.one-check').not(this).prop('checked', false);
    });
    /* Required la liste of checkbox */
    var requiredCheckboxes = $('.one-check[required]');
    requiredCheckboxes.prop('checked', false);
    requiredCheckboxes.on('change', function(){
        if(requiredCheckboxes.is(':checked')) {
            requiredCheckboxes.removeAttr('required');
        }
        else {
            requiredCheckboxes.attr('required', 'required');
        }
    });

    /******* TROUVER LOGEMENT *******/

    if(pageName === "trouver-logement") {
        if($('#loader').length) {
            //loader en cours
            setTimeout(function() {
                $('#etape_recherche').click();
            },5000); 
        }
    }

    /******* PROPOSER LOGEMENT *******/

    //Popup apparait si un étudiant clique sur Proposer un logement
    if(pageName === "proposer-logement") {
        $('body').css({
            'background-color': '#00aa9c',
            'color': 'white'
        }); 
        var statut = $('#id_statut').val();
        if(statut === "1") {
            $('.modal').css('color', 'black');
            $('#showPopUp').click();
            $('#popUp').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                return false;
            });
        } else if($('#loader').length) {
            //loader en cours
            setTimeout(function() {
                $('#etape_estimation').click();
            },5000); 
        }        
    }

    $('#ville_pl').on('change', function() {
        var id_ville = $(this).children("option:selected").val();
        $.ajax({
            type: "POST",
            url: "proposer-logement",
            data: {id_ville: id_ville},
            success  : function(data) {
                location.reload();
            }       
       });
    });

    /******* DETAIL LOGEMENT *******/

    //Popup apparait si l'utilisateur n'est pas connecté
    if(pageName === "logement") {
        var session_id = $('#session_id').val();
        if(session_id === "null") {
            $('#showPopUp').click();
            $('#popUp').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                return false;
            });
        } 
    }
 });