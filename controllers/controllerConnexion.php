<?php 
require_once('views/View.php');

class controllerConnexion {

    private $_view;
    private $_clientmanager;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->connexion();
        }
    }

    function isMail($var) {
        return filter_var($var, FILTER_VALIDATE_EMAIL);
    }

    private function connexion() {
        $generalError = "";
        $isSuccess = true;
        if(isset($_POST['signin'])) {
            $mail = $_POST['mail'];
            $pwd = $_POST['pwd'];

            if(!$this->isMail($mail)) {
                $generalError = "Il ne s'agit pas d'un mail";
                $isSuccess = false;
            }

            if($isSuccess) {
                $this->_clientmanager = new ClientManager();
                $connexionValide = $this->_clientmanager->connexionValide($mail, $pwd);
                if($connexionValide) {
                    header('Location: accueil');
                } else {
                    //erreur lors de la connexion
                    $generalError = "Mail ou mot de passe invalide";
                }
            }
        } else if(isset($_POST['deconnexion'])) {
            $_SESSION = array();
            session_destroy();
        } else {
            if(isset($_SESSION['ID'])) {
                header('Location: accueil');
            }
        }
        $this->_view = new View('Connexion');
        $this->_view->generate(array('generalError' => $generalError));
    }
}