<?php 
require_once('views/View.php');

class controllerLogement {

    private $_view;
    private $_logementmanager;
    private $_favorismanager;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->logement();
        }
    }

    private function logement() {

        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $actual_link = explode("/", $actual_link);
        $id = $actual_link[sizeof($actual_link) -1];
        
        $this->_logementmanager = new LogementManager();
        if($this->_logementmanager->logementExist($id)) {
            $logement = $this->_logementmanager->getLogement($id);

            if(isset($_POST['ajout_fav'])) {
                $this->_favorismanager = new FavorisManager();
                $insertion = $this->_favorismanager->insertionFavoris($_SESSION['ID'], $id);
                if($insertion) { 
                    $this->_view = new View('Logement');
                    $this->_view->generate(array('logement' => $logement, 'insertion' => $insertion));
                }
            } else if(isset($_POST['suppr_fav'])) {
                $this->_favorismanager = new FavorisManager();
                $suppression = $this->_favorismanager->suppressionFavoris($_SESSION['ID'], $id);
                if($suppression) { 
                    $this->_view = new View('Logement');
                    $this->_view->generate(array('logement' => $logement, 'suppression' => $suppression));
                }
            }
            
            $this->_view = new View('Logement');
            $this->_view->generate(array('logement' => $logement));
        } else {
            $this->_view = new View('Logement');
            $this->_view->generate(array('erreur' => 'Ce logement n\'existe pas'));
        }
    }
}
