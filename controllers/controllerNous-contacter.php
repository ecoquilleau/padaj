<?php 
require_once('views/View.php');

class controllerNous_contacter {

    private $_view;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->Nous_contacter();
        }
    }

    function isPhone($var) {

        return preg_match("/^[0-9 ]*$/", $var);
    }

    function isEmail($var) {
        return filter_var($var, FILTER_VALIDATE_EMAIL);
    }

    function verifyInput($var) {
        $var = trim($var);
        $var = stripslashes($var);
        $var = htmlspecialchars($var);

        return $var;
    }

    private function Nous_contacter() {
        $firstname = $name = $email = $phone = $message = "";
        $firstnameError = $nameError = $emailError = $phoneError = $messageError = "";
        $isSuccess = false;
        $emailTo = "contact@padaj.fr";
        if ($_SERVER["REQUEST_METHOD"] == "POST")
        {
            $firstname = $this->verifyInput($_POST["firstname"]);
            $name = $this->verifyInput($_POST["name"]);
            $email = $this->verifyInput($_POST["email"]);
            $phone = $this->verifyInput($_POST["phone"]);
            $message = $this->verifyInput($_POST["message"]);
            $isSuccess = true;
            $emailText = "";
        
            if (empty($firstname))
            {
                $firstnameError = "Nous avons besoin de ton prénom !";
                $isSuccess = false;
            } else {
                $emailText .= "Prénom: $firstname\n";
            }
            if (empty($name))
            {
                $nameError = "Nous avons besoin de ton nom !";
                $isSuccess = false;
            }  else {
                $emailText .= "Nom: $name\n";
            }
            if (empty($message))
            {
                $messageError =  "Ce message est vide !";
                $isSuccess = false;
            } else {
                $emailText .= "Message: $message\n";
            }
            if (!$this->isEmail($email)) {
                $emailError = "Ce n'est pas un bon email !";
                $isSuccess = false;
            } else {
                $emailText .= "email: $email\n";
            }
            if (!$this->isPhone($phone)) {
                $phoneError = "Ce n'est pas un bon numéro de téléphone !";
                $isSuccess = false;
            }
            else {
                $emailText .= "Téléphone: $phone\n";
            }
            if($isSuccess) {
                $headers = "From: $firstname $name <$email>\r\nReply-To: $email";
                mail($emailTo,"Un message de votre site", $emailText , $headers);
                $firstname = $name = $phone = $email = $message = "";
            }
            $this->_view = new View('Nous-contacter');
            $this->_view->generate(array('isSuccess' => $isSuccess, 'firstname' => $firstname, 'firstnameError' => $firstnameError, 'name' => $name, 'nameError' => $nameError, 
            'email' => $email, 'emailError' => $emailError, 'phone' => $phone, 'phoneError' => $phoneError, 'message' => $message, 'messageError' => $messageError));
        
        } else {
            $this->_view = new View('Nous-contacter');
            $this->_view->generate(array('isSuccess' => $isSuccess, 'firstname' => $firstname, 'firstnameError' => $firstnameError, 'name' => $name, 'nameError' => $nameError, 
            'email' => $email, 'emailError' => $emailError, 'phone' => $phone, 'phoneError' => $phoneError, 'message' => $message, 'messageError' => $messageError));
        }
    }

}
