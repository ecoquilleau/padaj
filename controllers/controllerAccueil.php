<?php 
require_once('views/View.php');

class controllerAccueil {

    private $_view;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->accueil();
        }
    }

    private function accueil() {
        /* proposer-logement */
        unset($_SESSION['etape_pl']);
        unset($_SESSION['save_id_ville']);
        unset($_SESSION['quartiers']);
        unset($_SESSION['id_quartier']);
        unset($_SESSION['adresse']);
        unset($_SESSION['surface_totale']);
        unset($_SESSION['surface_chambre']);
        unset($_SESSION['duree']);
        unset($_SESSION['servicesList']);
        unset($_SESSION['quartier']);
        unset($_SESSION['prix_location']);
        unset($_SESSION['insertionsValides']);
        $this->_view = new View('Accueil');
        $this->_view->generate(array());
    }
}
