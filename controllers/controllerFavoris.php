<?php 
require_once('views/View.php');

class controllerFavoris {

    private $_view;
    private $_favorismanager;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->favoris();
        }
    }

    private function favoris() {
        $this->_favorismanager = new FavorisManager();
        $favoris = $this->_favorismanager->getFavoris($_SESSION['ID']);
        $this->_view = new View('Favoris');
        $this->_view->generate(array('favoris' => $favoris));
    }
}