<?php 
require_once('views/View.php');

class controllerFaq {
    private $_view;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->faq();
        }
    }

    private function faq() {
        $this->_view = new View('Faq');
        $this->_view->generate(array());
    }
}