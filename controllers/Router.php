<?php
require_once(dirname(__DIR__).'/views/View.php');

class Router {

    private $_ctrl;
    private $_view;

    public function routerReq() {
        try {
            spl_autoload_register(function($nameClass) {
                require_once('models/'.$nameClass.'.php');
            });

            $url = '';
            if(isset($_GET['url'])) {

                $url = explode('/', filter_var($_GET['url'], FILTER_SANITIZE_URL));

                $controller = ucfirst(strtolower($url[0]));
                $controllerClass = 'controller'.$controller;
                $controllerFile = 'controllers/'.$controllerClass.'.php';

                if(file_exists($controllerFile)) {
                    require_once($controllerFile);
                    $controllerClassTab = explode('-', $controllerClass);
                    $realControllerClass = '';
                    if(sizeof($controllerClassTab) > 1) { //controllerClass possède un -
                        for($i = 0; $i < sizeof($controllerClassTab); $i++) {
                            $realControllerClass .= $controllerClassTab[$i];
                            if($i !== sizeof($controllerClassTab) - 1) {
                                $realControllerClass .= '_';
                            }
                        }   
                        $this->_ctrl = new $realControllerClass($url);
                    } else {
                        $this->_ctrl = new $controllerClass($url);
                    }
                } else {
                    throw new Exception('Page introuvable');
                }
            } else {
                require_once('controllers/controllerAccueil.php');
                $this->_ctrl = new controllerAccueil($url);
            }
        } catch(Exception $e) {
            $erreur = $e->getMessage();
            $this->_view = new View('404');
            $this->_view->generate(array('erreur' => $erreur));
        }
    }
}