<?php 
require_once('views/View.php');

class controllerTrouver_logement {

    private $_view;
    private $_villemanager;
    private $_logementmanager;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->trouver_logement();
        }
    }

    private function trouver_logement() {
        
        if(isset($_POST['trouver_logement'])) { //etape recherche terminée
            $id_ville = $_POST['ville'];
            $budget = $_POST['budget'];
            $service = $_POST['service'];
            $this->_logementmanager = new LogementManager(); 
            $_SESSION['logements'] = $this->_logementmanager->getLogementsRecherches($id_ville, $budget, $service);
            $this->_view = new View('Trouver-logement');
            $this->_view->generate(array());
        } else {
            $this->_view = new View('Trouver-logement');
            $this->_view->generate(array());
        }

    }
}
