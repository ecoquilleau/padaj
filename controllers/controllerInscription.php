<?php 
require_once('views/View.php');

class controllerInscription {

    private $_view;
    private $_clientmanager;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->inscription();
        }
    }

    function isPhone($var) {
        return preg_match("/^[0-9 ]*$/", $var);
    }

    function isMail($var) {
        return filter_var($var, FILTER_VALIDATE_EMAIL);
    }

    private function inscription() {
        $name = $firstname = $birthday = $mail = $status = $mail = $phone = "";
        $generalError = $nameError = $firstnameError = $birthdayError = $mailError = $phoneError = $pwdError = "";
        $isSuccess = true;
        if(isset($_POST['register'])) {
            $pwd = $_POST['pwd'];
            $pwd_conf = $_POST['pwd_conf'];

            //recupère les autres données
            $name = $_POST['name'];
            $firstname = $_POST['firstname'];
            $birthday = $_POST['birthday'];
            $status = $_POST['status'];
            $mail = $_POST['mail'];
            $phone = $_POST['phone'];

            if($pwd === $pwd_conf) {
                //hash du password
                $pwd = password_hash($pwd, PASSWORD_DEFAULT);
                
                if (!$this->isMail($mail)) {
                    $mailError = "Mail incorrect";
                    $isSuccess = false;
                }
                if (!$this->isPhone($phone)) {
                    $phoneError = "Téléphone incorrect";
                    $isSuccess = false;
                }

                if($isSuccess) {
                    //check si mail inexistant
                    $this->_clientmanager = new ClientManager();
                    $mail_existant = $this->_clientmanager->mailExistant($mail);
                    if(!$mail_existant) {
                        $insertion = $this->_clientmanager->insertionClient($name, $firstname, $birthday, $status, $mail, $phone, $pwd);
                        if($insertion) {
                            $this->_view = new View('Connexion');
                            $this->_view->generate(array('insertion' => true));
                        } else {
                            //problème lors de l'inscription                       
                            $generalError = "Problème survenu lors de l'inscription";
                        }
                    } else {
                        //mail utilisé
                        $generalError = "Ce mail est déjà associé un compte existant";
                    }

                }
            } else {
                $pwdError = "Mot de passe différent";
            }
        } else {
            if(isset($_SESSION['ID'])) {
                header('Location: accueil');
            }
            if(isset($_SESSION['PL'])) { //arrive de proposer logement
                $generalError = "Pour proposer un logement, un compte est nécéssaire";
                unset($_SESSION['PL']);
            }
        }
        $this->_view = new View('Inscription');
        $this->_view->generate(array('name' => $name, 'firstname' => $firstname, 'birthday' => $birthday, 'mail' => $mail, 'status' => $status, 'mail' => $mail, 'phone' => $phone,
            'generalError' => $generalError, 'nameError' => $nameError, 'firstnameError' => $firstnameError, 'birthdayError' => $birthdayError, 'mailError' => $mailError, 'phoneError' => $phoneError, 'pwdError' => $pwdError));
    }
}