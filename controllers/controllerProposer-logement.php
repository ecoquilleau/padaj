<?php 
require_once('views/View.php');

class controllerProposer_logement {

    private $_view;
    private $_quartiermanager;
    private $_logementmanager;
    private $_estbailleurmanager;
    private $_possedeservicemanager;

    public function __construct($url) {
        if(!isset($url) && count($url) > 1) {
            throw new Exception('Page introuvable');
        } else {
            $this->proposer_logement();
        }
    }

    private function proposer_logement() {
        //récupère l'étape courante
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $actual_link = explode("/", $actual_link);
        $num_etape = $actual_link[sizeof($actual_link) -1];
        $_SESSION['etape_pl'] = $num_etape;

        $this->_view = new View('Proposer-logement');

        if($_SESSION['etape_pl'] === "2") {
            if(isset($_SESSION['insertionsValides'])) {
                header('Location:'. URL .'proposer-logement/4');
            } else if(isset($_POST['etape_localisation']) || isset($_SESSION['id_quartier'])) {
                if(!isset($_SESSION['refresh']) && isset($_POST['quartier'])) {
                    $_SESSION['id_quartier'] = $_POST['quartier'];
                    $_SESSION['adresse'] = $_POST['adresse'];
                }
                $this->_view->generate(array());
            } else {
                header('Location:'. URL .'proposer-logement/1');
            }

        } else if($_SESSION['etape_pl'] === "3") {

            if(isset($_SESSION['insertionsValides'])) {
                header('Location:'. URL .'proposer-logement/4');
            } else if(isset($_POST['etape_description']) || isset($_SESSION['surface_totale'])) {
                if(!isset($_SESSION['refresh']) && isset($_POST['surface_totale'])) {
                    $_SESSION['surface_totale'] = $_POST['surface_totale'];
                    $_SESSION['surface_chambre'] = $_POST['surface_chambre'];
                    $_SESSION['duree'] = $_POST['duree']; //TODO
                    if(isset($_POST['service'])) {  
                        $_SESSION['servicesList'] = implode(',', $_POST['service']);
                    } else {
                        $_SESSION['servicesList'] = '';
                    }
                }
                $this->_view->generate(array());
            } else {
                header('Location:'. URL .'proposer-logement/2');
            }
        
        } else if($_SESSION['etape_pl'] === "4") {
            
            if(isset($_POST['etape_estimation']) || isset($_SESSION['surface_totale'])) {
                if(!isset($_SESSION['refresh'])) {
                    if(!isset($_SESSION['insertionsValides'])) {
                        $this->_quartiermanager = new QuartierManager();
                        $_SESSION['quartier'] = $this->_quartiermanager->getQuartier($_SESSION['id_quartier']);
                        $prix_m2 = $this->_quartiermanager->getPrixM2($_SESSION['id_quartier']);
                        $_SESSION['prix_location'] = $_SESSION['surface_totale']*$prix_m2;
                        $this->_logementmanager = new LogementManager();
                        $id_logement_insert = $this->_logementmanager->insertionLogement($_SESSION['id_quartier'], $_SESSION['adresse'], $_SESSION['surface_totale'], $_SESSION['surface_chambre'], $_SESSION['prix_location']);
                        $_SESSION['insertionsValides'] = true;
                        if($id_logement_insert) {
                            $this->_estbailleurmanager = new Est_bailleurManager();
                            $insertion_eb = $this->_estbailleurmanager->insertionBailleur($_SESSION['ID'], $id_logement_insert);
                            if(!$insertion_eb) {
                                $_SESSION['insertionsValides'] = false;
                            }
                            if($_SESSION['servicesList'] != '') {
                                $this->_possedeservicemanager = new Possede_serviceManager();
                                $id_services = explode(',', $_SESSION['servicesList']);
                                foreach($id_services as $id_service) {
                                    $insertion_ps = $this->_possedeservicemanager->insertionServicePossede($id_logement_insert, $id_service);
                                    if(!$insertion_ps) {
                                        $_SESSION['insertionsValides'] = false;
                                    }
                                }
                            }
                        } else {
                            $_SESSION['insertionsValides'] = false;
                        }
                    }
                }
                $this->_view->generate(array());

            } else {
                header('Location:'. URL .'proposer-logement/3');
            }
        
        } else {

            if(isset($_SESSION['insertionsValides'])) {
                header('Location:'. URL .'proposer-logement/4');
            }
            $this->_view->generate(array());

        }
    }
}