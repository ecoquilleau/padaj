<?php

class QuartierManager extends Model {

    public function getQuartiers() {
        $this->getBdd();
        return $this->getAll('quartier', 'Quartier');
    }
    
    public function getQuartier($id) {
        $this->getBdd();
        return $this->getOne('quartier', 'Quartier', $id);
    }

    public function getQuartiersOfVille($id_ville) {
        $_bdd = $this->getBdd();
        $var = [];
        $req = $_bdd->prepare('SELECT * FROM quartier WHERE ID_VILLE = :ID_VILLE');
        $req->execute(array('ID_VILLE' => $id_ville));
        while($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $var[] = new Quartier($data);
        }
        return $var;
    }

    public function getPrixM2($id) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT PRIX_M2 FROM quartier WHERE ID = :ID');
        $req->execute(array('ID' => $id));
        $service = $req->fetch();
        $req->closeCursor();
        return $service['PRIX_M2'];
    }
}