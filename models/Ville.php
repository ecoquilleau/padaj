<?php 

class Ville {

    private $_ID;
    private $_INTITULE;
    private $_CODE_POSTAL;

    public function __construct(array $data) {
        $this->allSetters($data);
    }

    public function allSetters(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.$key;
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->_ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->_ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getINTITULE()
    {
        return $this->_INTITULE;
    }

    /**
     * @param mixed $INTITULE
     */
    public function setINTITULE($INTITULE)
    {
        $this->_INTITULE = $INTITULE;
    }

    /**
     * @return mixed
     */
    public function getCODE_POSTAL()
    {
        return $this->_CODE_POSTAL;
    }

    /**
     * @param mixed $CODE_POSTAL
     */
    public function setCODE_POSTAL($CODE_POSTAL)
    {
        $this->_CODE_POSTAL = $CODE_POSTAL;
    }
}