<?php 

class Client {

    private $_ID;
    private $_ID_STATUT;
    private $_NOM;
    private $_PRENOM;
    private $_DATE_DE_NAISSANCE;
    private $_MAIL;
    private $_TELEPHONE;
    private $_PASSWORD;

    public function __construct(array $data) {
        $this->allSetters($data);
    }

    public function allSetters(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.$key;
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->_ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->_ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getID_STATUT()
    {
        return $this->_ID_VILLE;
    }

    /**
     * @param mixed $ID_STATUT
     */
    public function setID_STATUT($ID_STATUT)
    {
        $this->_ID_STATUT = $ID_STATUT;
    }

    /**
     * @return mixed
     */
    public function getNOM()
    {
        return $this->_NOM;
    }

    /**
     * @param mixed $NOM
     */
    public function setNOM($NOM)
    {
        $this->_NOM = $NOM;
    }

    /**
     * @return mixed
     */
    public function getPRENOM()
    {
        return $this->_PRENOM;
    }

    /**
     * @param mixed $PRENOM
     */
    public function setPRENOM($PRENOM)
    {
        $this->_PRENOM = $PRENOM;
    }

    /**
     * @return mixed
     */
    public function getDATE_DE_NAISSANCE()
    {
        return $this->_DATE_DE_NAISSANCE;
    }

    /**
     * @param mixed $DATE_DE_NAISSANCE
     */
    public function setDATE_DE_NAISSANCE($DATE_DE_NAISSANCE)
    {
        $this->_DATE_DE_NAISSANCE = $DATE_DE_NAISSANCE;
    }

    /**
     * @return mixed
     */
    public function getMAIL()
    {
        return $this->_MAIL;
    }

    /**
     * @param mixed $MAIL
     */
    public function setMAIL($MAIL)
    {
        $this->_MAIL = $MAIL;
    }

    /**
     * @return mixed
     */
    public function getTELEPHONE()
    {
        return $this->_TELEPHONE;
    }

    /**
     * @param mixed $TELEPHONE
     */
    public function setTELEPHONE($TELEPHONE)
    {
        $this->_TELEPHONE = $TELEPHONE;
    }

    /**
     * @return mixed
     */
    public function getPASSWORD()
    {
        return $this->_PASSWORD;
    }

    /**
     * @param mixed $PASSWORD
     */
    public function setPASSWORD($PASSWORD)
    {
        $this->_PASSWORD = $PASSWORD;
    }
}