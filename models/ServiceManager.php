<?php

class ServiceManager extends Model {

    public function getServices() {
        $this->getBdd();
        return $this->getAll('service', 'Service');
    }

    public function getService($id) {
        $this->getBdd();
        return $this->getOne('service', 'Service', $id);
    }

    public function getServicesPrecis($services_id) {
        $services_valides = array();
        foreach($services_id as $service) {
            array_push($services_valides, $this->getService($service['ID_SERVICE']));
        }
        return $services_valides;
    }

    public function getRemise($id) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT REMISE FROM service WHERE ID = :ID');
        $req->execute(array('ID' => $id));
        $service = $req->fetch();
        $req->closeCursor();
        return $service['REMISE'];
    }
}