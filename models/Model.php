<?php

abstract class Model {

    private static $_bdd;

    private static function setBdd() {
        $ini = parse_ini_file(dirname(__DIR__).'/config/config.ini');
        $db_host = $ini['db_host'];
        $db_name = $ini['db_name'];
        $db_user = $ini['db_user'];
        $db_password = $ini['db_password'];

        self::$_bdd = new PDO('mysql:host='.$db_host.';dbname='.$db_name.';charset=utf8', $db_user, $db_password);
        self::$_bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }

    protected function getBdd() {
        if(self::$_bdd == null) {
            $this->setBdd();
        }
        return self::$_bdd;
    }
    
    protected function getAll($table, $obj) {
        $var = [];
        $req = self::$_bdd->prepare('SELECT * FROM '.$table);
        $req->execute();
        while($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $var[] = new $obj($data);
        }
        return $var;
        $req->closeCursor();
    }

    protected function getOne($table, $obj, $id) {
        $var = [];
        $req = self::$_bdd->prepare('SELECT * FROM '.$table.' WHERE ID = :ID');
        $req->execute(array('ID' => $id));
        while($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $var[] = new $obj($data);
        }
        return $var;
        $req->closeCursor();
    }
}
