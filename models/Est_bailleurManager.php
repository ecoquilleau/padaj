<?php

class Est_bailleurManager extends Model {

    public function getBailleurID($id_logement) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT ID_CLIENT FROM est_bailleur WHERE ID_LOGEMENT = :ID_LOGEMENT');
        $req->execute(array('ID_LOGEMENT' => $id_logement));
        $bailleur = $req->fetch();
        $req->closeCursor();
        return $bailleur['ID_CLIENT'];
    }

    public function insertionBailleur($id_client, $id_logement) {
        $_bdd = $this->getBdd();
        $insertion = true;
        $req = $_bdd->prepare('INSERT INTO est_bailleur(ID_CLIENT, ID_LOGEMENT) VALUES (:ID_CLIENT, :ID_LOGEMENT)');
        $req->execute(array('ID_CLIENT' => $id_client, 'ID_LOGEMENT' => $id_logement));
        $count = $req->rowCount();
        if($count === -1) {
            $insertion = false;
        }
        return $insertion;
    }
}