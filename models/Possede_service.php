<?php

class Possede_service {

    private $_ID_LOGEMENT;
    private $_ID_SERVICE;

    public function __construct(array $data) {
        $this->allSetters($data);
    }

    public function allSetters(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.$key;
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_LOGEMENT()
    {
        return $this->_ID_LOGEMENT;
    }

    /**
     * @param mixed $ID_LOGEMENT
     */
    public function setID_LOGEMENT($ID_LOGEMENT)
    {
        $this->_ID_LOGEMENT = $ID_LOGEMENT;
    }

    /**
     * @return mixed
     */
    public function getID_SERVICE()
    {
        return $this->_ID_SERVICE;
    }

    /**
     * @param mixed $ID_SERVICE
     */
    public function setID_SERVICE($ID_SERVICE)
    {
        $this->_ID_SERVICE = $ID_SERVICE;
    }
}