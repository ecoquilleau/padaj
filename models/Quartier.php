<?php 

class Quartier {

    private $_ID;
    private $_ID_VILLE;
    private $_INTITULE;
    private $_PRIX_M2;

    public function __construct(array $data) {
        $this->allSetters($data);
    }

    public function allSetters(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.$key;
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->_ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->_ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getID_VILLE()
    {
        return $this->_ID_VILLE;
    }

    /**
     * @param mixed $ID_VILLE
     */
    public function setID_VILLE($ID_VILLE)
    {
        $this->_ID_VILLE = $ID_VILLE;
    }

    /**
     * @return mixed
     */
    public function getINTITULE()
    {
        return $this->_INTITULE;
    }

    /**
     * @param mixed $INTITULE
     */
    public function setINTITULE($INTITULE)
    {
        $this->_INTITULE = $INTITULE;
    }

    /**
     * @return mixed
     */
    public function getPRIX_M2()
    {
        return $this->_PRIX_M2;
    }

    /**
     * @param mixed $PRIX_M2
     */
    public function setPRIX_M2($PRIX_M2)
    {
        $this->_PRIX_M2 = $PRIX_M2;
    }
}