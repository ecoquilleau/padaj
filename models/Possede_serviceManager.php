<?php

class Possede_serviceManager extends Model {

    public function getServicesOfLogement($id_logement) {
        $_bdd = $this->getBdd();
        $var = [];
        $req = $_bdd->prepare('SELECT ID_SERVICE FROM possede_service WHERE ID_LOGEMENT = :ID_LOGEMENT');
        $req->execute(array('ID_LOGEMENT' => $id_logement));
        while($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $var[] = $data;
        }
        return $var;
    }

    public function insertionServicePossede($id_logement, $id_service) {
        $_bdd = $this->getBdd();
        $insertion = true;
        $req = $_bdd->prepare('INSERT INTO possede_service(ID_LOGEMENT, ID_SERVICE) VALUES (:ID_LOGEMENT, :ID_SERVICE)');
        $req->execute(array('ID_LOGEMENT' => $id_logement, 'ID_SERVICE' => $id_service));
        $count = $req->rowCount();
        if($count === -1) {
            $insertion = false;
        }
        return $insertion;
    }
}