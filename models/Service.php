<?php 

class Service {

    private $_ID;
    private $_INTITULE;
    private $_FREQUENCE;
    private $_REMISE;

    public function __construct(array $data) {
        $this->allSetters($data);
    }

    public function allSetters(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.$key;
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->_ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->_ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getINTITULE()
    {
        return $this->_INTITULE;
    }

    /**
     * @param mixed $INTITULE
     */
    public function setINTITULE($INTITULE)
    {
        $this->_INTITULE = $INTITULE;
    }

    /**
     * @return mixed
     */
    public function getFREQUENCE()
    {
        return $this->_FREQUENCE;
    }

    /**
     * @param mixed $FREQUENCE
     */
    public function setFREQUENCE($FREQUENCE)
    {
        $this->_FREQUENCE = $FREQUENCE;
    }

    /**
     * @return mixed
     */
    public function getREMISE()
    {
        return $this->_REMISE;
    }

    /**
     * @param mixed $REMISE
     */
    public function setREMISE($REMISE)
    {
        $this->_REMISE = $REMISE;
    }
}