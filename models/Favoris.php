<?php

class Favoris {

    private $_ID_CLIENT;
    private $_ID_LOGEMENT;

    public function __construct(array $data) {
        $this->allSetters($data);
    }

    public function allSetters(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.$key;
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_CLIENT()
    {
        return $this->_ID_CLIENT;
    }

    /**
     * @param mixed $ID_CLIENT
     */
    public function setID_CLIENT($ID_CLIENT)
    {
        $this->_ID_CLIENT = $ID_CLIENT;
    }

    /**
     * @return mixed
     */
    public function getID_LOGEMENT()
    {
        return $this->_ID_LOGEMENT;
    }

    /**
     * @param mixed $ID_LOGEMENT
     */
    public function setID_LOGEMENT($ID_LOGEMENT)
    {
        $this->_ID_LOGEMENT = $ID_LOGEMENT;
    }
}