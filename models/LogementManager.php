<?php

class LogementManager extends Model {

    public function getLogements() {
        $this->getBdd();
        return $this->getAll('logement', 'Logement');
    }

    public function getLogement($id) {
        $this->getBdd();
        return $this->getOne('logement', 'Logement', $id);
    }

    public function logementExist($id) {
        $_bdd = $this->getBdd();
        $exist = true;
        $req = $_bdd->prepare('SELECT ID FROM logement WHERE ID = :ID');
        $req->execute(array('ID' => $id));
        $count = $req->rowCount();
        if($count === 0) {
            $exist = false;
        }
        return $exist;
    }

    public function getLogementsOfVille($id_ville) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT l.* FROM logement as l INNER JOIN quartier as q ON q.ID = l.ID_QUARTIER INNER JOIN ville as v ON v.ID = q.ID_VILLE WHERE v.ID = :ID_VILLE');
        $req->execute(array('ID_VILLE' => $id_ville));
        $count = $req->rowCount();
        if($count === 0) {
            $var = null;
        } else {          
            while($data = $req->fetch(PDO::FETCH_ASSOC)) {
                $var[] = new Logement($data);
            }
        }
        return $var;
        $req->closeCursor();
    }

    public function getRemiseMax($id_logement) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT SUM(s.REMISE) as REMISE_MAX FROM logement as l
            INNER JOIN possede_service as ps ON l.ID = ps.ID_LOGEMENT
            INNER JOIN service as s ON ps.ID_SERVICE = s.ID
            WHERE l.ID = :ID_LOGEMENT');
        $req->execute(array('ID_LOGEMENT' => $id_logement));
        $service = $req->fetch();
        $req->closeCursor();
        return $service['REMISE_MAX'];
    }

    public function getLogementsRecherches($id_ville, $budget, $service) {
        $logements = $this->getLogementsOfVille($id_ville);
        $logementsRecherches = array();
        if($service === '1') /* Services acceptés */ {
            foreach($logements as $logement) {
                $remiseMax = $this->getRemiseMax($logement->getID());
                $prixMin = $budget + $remiseMax;
                if($prixMin > 0) {  
                    if($logement->getPRIX_LOCATION() <= $prixMin) {
                        array_push($logementsRecherches, $logement);
                    }
                } else {
                    array_push($logementsRecherches, $logement);
                }
            }
        } else /* Services refusés */ {
            foreach($logements as $logement) {
                if($logement->getPRIX_LOCATION() <= $budget) {
                    array_push($logementsRecherches, $logement);
                }
            }
        }
        return $logementsRecherches;
    }

    public function insertionLogement($id_quartier, $adresse, $surface_totale, $surface_chambre, $prix_location) {
        $_bdd = $this->getBdd();
        $insertion = true;
        $req = $_bdd->prepare('INSERT INTO logement(ID_QUARTIER, ADRESSE, SURFACE_TOTALE, SURFACE_CHAMBRE, PRIX_LOCATION)
                VALUES (:ID_QUARTIER, :ADRESSE, :SURFACE_TOTALE, :SURFACE_CHAMBRE, :PRIX_LOCATION)');
        $req->execute(array('ID_QUARTIER' => $id_quartier, 'ADRESSE' => $adresse, 'SURFACE_TOTALE' => $surface_totale, 'SURFACE_CHAMBRE' => $surface_chambre, 'PRIX_LOCATION' => $prix_location));
        $count = $req->rowCount();
        if($count === -1) {
            $insertion = false;
        }
        if($insertion) {
            return $_bdd->lastInsertID();
        }
        return false;
    }
    
}