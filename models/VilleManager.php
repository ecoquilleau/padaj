<?php

class VilleManager extends Model {

    public function getVilles() {
        $this->getBdd();
        return $this->getAll('ville', 'Ville');
    }
    
    public function getVille($id) {
        $this->getBdd();
        return $this->getOne('ville', 'Ville', $id);
    }

    public function getVilleOfQuartier($id_quartier) {
        $_bdd = $this->getBdd();
        $var = [];
        $req = $_bdd->prepare('SELECT v.* FROM ville as v INNER JOIN quartier as q ON q.ID_VILLE = v.ID WHERE q.ID = :ID_QUARTIER');
        $req->execute(array('ID_QUARTIER' => $id_quartier));
        while($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $var[] = new Ville($data);
        }
        return $var;
        $req->closeCursor();
    }
}