<?php

class ClientManager extends Model {

    public function getClients() {
        $this->getBdd();
        return $this->getAll('client', 'Client');
    }

    public function getClient($id) {
        $this->getBdd();
        return $this->getOne('client', 'Client', $id);
    }

    public function mailExistant($mail) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT * FROM client WHERE MAIL = :MAIL');
        $req->execute(array('MAIL' => $mail));
        $count = $req->rowCount();
        if($count != 0) {
            return true;
        }
        return false;
    }

    public function insertionClient($nom, $prenom, $date_naissance, $statut, $mail, $telephone, $pwd) {
        $_bdd = $this->getBdd();
        $insertion = true;
        $req = $_bdd->prepare('INSERT INTO client(ID_STATUT, NOM, PRENOM, DATE_DE_NAISSANCE, MAIL, TELEPHONE, PASSWORD)
                VALUES (:ID_STATUT, :NOM, :PRENOM, :DATE_DE_NAISSANCE, :MAIL, :TELEPHONE, :PASSWORD)');
        $req->execute(array('ID_STATUT' => $statut, 'NOM' => $nom, 'PRENOM' => $prenom, 'DATE_DE_NAISSANCE' => $date_naissance, 'MAIL' => $mail, 'TELEPHONE' => $telephone, 'PASSWORD' => $pwd));
        $count = $req->rowCount();
        if($count === -1) {
            $insertion = false;
        }
        return $insertion;
    }

    public function getHash($mail) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT PASSWORD FROM client WHERE MAIL = :MAIL');
        $req->execute(array('MAIL' => $mail));
        $hash = $req->fetch();
        $req->closeCursor();
        return $hash['PASSWORD'];
    }

    public function connexionValide($mail, $pwd) {
        $PASSWORD_HASH = $this->getHash($mail);
        if(password_verify($pwd, $PASSWORD_HASH)) {     
            $_bdd = $this->getBdd();
            $req = $_bdd->prepare('SELECT * FROM client WHERE MAIL = :MAIL AND PASSWORD = :PASSWORD');
            $req->execute(array('MAIL' => $mail, 'PASSWORD' => $PASSWORD_HASH));
            $count = $req->rowCount();
            if($count === 1) {
                while($dataReq = $req->fetch()) {
                    $_SESSION['ID'] = $dataReq['ID'];
                    $_SESSION['NOM'] = $dataReq['NOM'];
                    $_SESSION['PRENOM'] = $dataReq['PRENOM'];
                    $_SESSION['ID_STATUT'] = $dataReq['ID_STATUT'];
                }
                return true;
            }
            return false;
        }
        return false;
    }

}