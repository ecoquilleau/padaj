<?php

class FavorisManager extends Model {

    public function getFavoris($id_client) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT * FROM favoris WHERE ID_CLIENT = :ID_CLIENT');
        $req->execute(array('ID_CLIENT' => $id_client));
        if($req->rowCount() > 0) {               
            while($data = $req->fetch(PDO::FETCH_ASSOC)) {
                $var[] = new Favoris($data);
            }
            return $var;
            $req->closeCursor();
        } else {
            return null;
        }
    }

    public function estFavoris($id_client, $id_logement) {
        $_bdd = $this->getBdd();
        $req = $_bdd->prepare('SELECT COUNT(*) FROM favoris WHERE ID_CLIENT = :ID_CLIENT AND ID_LOGEMENT = :ID_LOGEMENT');
        $req->execute(array('ID_CLIENT' => $id_client, 'ID_LOGEMENT' => $id_logement));
        $fav_existe = $req->fetch();
        if($fav_existe[0] >= '1') {
            return true;
        }
        return false;
    }

    public function insertionFavoris($id_client, $id_logement) {
        $_bdd = $this->getBdd();
        $insertion = true;
        $req = $_bdd->prepare('INSERT INTO favoris(ID_CLIENT, ID_LOGEMENT) VALUES (:ID_CLIENT, :ID_LOGEMENT)');
        $req->execute(array('ID_CLIENT' => $id_client, 'ID_LOGEMENT' => $id_logement));
        $count = $req->rowCount();
        if($count === -1) {
            $insertion = false;
        }
        return $insertion;
    }

    public function suppressionFavoris($id_client, $id_logement) {
        $_bdd = $this->getBdd();
        $suppression = true;
        $req = $_bdd->prepare('DELETE FROM favoris WHERE ID_CLIENT = :ID_CLIENT AND ID_LOGEMENT = :ID_LOGEMENT');
        $req->execute(array('ID_CLIENT' => $id_client, 'ID_LOGEMENT' => $id_logement));
        $count = $req->rowCount();
        if($count === -1) {
            $suppression = false;
        }
        return $suppression;
    }
}