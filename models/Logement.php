<?php 

class Logement {

    private $_ID;
    private $_ID_QUARTIER;
    private $_ADRESSE;
    private $_SURFACE_TOTALE;
    private $_SURFACE_CHAMBRE;
    private $_PRIX_LOCATION;

    public function __construct(array $data) {
        $this->allSetters($data);
    }

    public function allSetters(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.$key;
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->_ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->_ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getID_QUARTIER()
    {
        return $this->_ID_QUARTIER;
    }

    /**
     * @param mixed $ID_QUARTIER
     */
    public function setID_QUARTIER($ID_QUARTIER)
    {
        $this->_ID_QUARTIER = $ID_QUARTIER;
    }

    /**
     * @return mixed
     */
    public function getADRESSE()
    {
        return $this->_ADRESSE;
    }

    /**
     * @param mixed $ADRESSE
     */
    public function setADRESSE($ADRESSE)
    {
        $this->_ADRESSE = $ADRESSE;
    }

    /**
     * @return mixed
     */
    public function getSURFACE_TOTALE()
    {
        return $this->_SURFACE_TOTALE;
    }

    /**
     * @param mixed $SURFACE_TOTALE
     */
    public function setSURFACE_TOTALE($SURFACE_TOTALE)
    {
        $this->_SURFACE_TOTALE = $SURFACE_TOTALE;
    }

    /**
     * @return mixed
     */
    public function getSURFACE_CHAMBRE()
    {
        return $this->_SURFACE_CHAMBRE;
    }

    /**
     * @param mixed $SURFACE_CHAMBRE
     */
    public function setSURFACE_CHAMBRE($SURFACE_CHAMBRE)
    {
        $this->_SURFACE_CHAMBRE = $SURFACE_CHAMBRE;
    }

    /**
     * @return mixed
     */
    public function getPRIX_LOCATION()
    {
        return $this->_PRIX_LOCATION;
    }

    /**
     * @param mixed $PRIX_LOCATION
     */
    public function setPRIX_LOCATION($PRIX_LOCATION)
    {
        $this->_PRIX_LOCATION = $PRIX_LOCATION;
    }
}