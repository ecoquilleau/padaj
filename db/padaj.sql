-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 22 mars 2020 à 17:16
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `padaj`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_STATUT` int(11) NOT NULL,
  `NOM` varchar(50) NOT NULL,
  `PRENOM` varchar(50) NOT NULL,
  `DATE_DE_NAISSANCE` date NOT NULL,
  `MAIL` varchar(255) NOT NULL,
  `TELEPHONE` varchar(50) NOT NULL,
  `PASSWORD` longtext NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ID_STATUT` (`ID_STATUT`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`ID`, `ID_STATUT`, `NOM`, `PRENOM`, `DATE_DE_NAISSANCE`, `MAIL`, `TELEPHONE`, `PASSWORD`) VALUES
(1, 1, 'COQUILLEAU', 'Eliot', '2020-03-04', 'a@a.a', '0600000000', '$2y$10$iOWW.5TGUV91BzjxmcnXUuS3BGDoW0GV4z4vpPtvnKcSpuQlpDg96'),
(2, 2, 'Je suis', 'Famille', '2020-03-26', 'b@b.b', '0600000000', '$2y$10$wcUFij7cZtuyRC9EehVmAOZuQnRZO5LIFSjSozSfjjbqXvwxJ3YRW'),
(3, 3, 'Je suis', 'Sénior', '1943-02-27', 'c@c.c', '0610101010', '$2y$10$mGqQuy4dK/SKmWXvqyJJ/uor9/79Yy.IqoAZPVEzwZ0y1TFnyG676');

-- --------------------------------------------------------

--
-- Structure de la table `est_bailleur`
--

DROP TABLE IF EXISTS `est_bailleur`;
CREATE TABLE IF NOT EXISTS `est_bailleur` (
  `ID_CLIENT` int(11) NOT NULL,
  `ID_LOGEMENT` int(11) NOT NULL,
  KEY `FK_ID_CLIENT_B` (`ID_CLIENT`),
  KEY `FK_ID_LOGEMENT_B` (`ID_LOGEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `est_bailleur`
--

INSERT INTO `est_bailleur` (`ID_CLIENT`, `ID_LOGEMENT`) VALUES
(2, 24),
(2, 25),
(2, 26),
(3, 1),
(3, 2),
(3, 3),
(3, 9),
(3, 18),
(3, 19),
(3, 22),
(3, 23),
(3, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 55),
(2, 56),
(2, 58),
(2, 59),
(2, 60),
(2, 61);

-- --------------------------------------------------------

--
-- Structure de la table `est_locataire`
--

DROP TABLE IF EXISTS `est_locataire`;
CREATE TABLE IF NOT EXISTS `est_locataire` (
  `ID_CLIENT` int(11) NOT NULL,
  `ID_LOGEMENT` int(11) NOT NULL,
  KEY `FK_ID_CLIENT_L` (`ID_CLIENT`),
  KEY `FK_ID_LOGEMENT_L` (`ID_LOGEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

DROP TABLE IF EXISTS `favoris`;
CREATE TABLE IF NOT EXISTS `favoris` (
  `ID_CLIENT` int(11) NOT NULL,
  `ID_LOGEMENT` int(11) NOT NULL,
  KEY `FK_ID_CLIENT_F` (`ID_CLIENT`),
  KEY `FK_ID_LOGEMENT_F` (`ID_LOGEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `favoris`
--

INSERT INTO `favoris` (`ID_CLIENT`, `ID_LOGEMENT`) VALUES
(2, 1),
(2, 24);

-- --------------------------------------------------------

--
-- Structure de la table `logement`
--

DROP TABLE IF EXISTS `logement`;
CREATE TABLE IF NOT EXISTS `logement` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_QUARTIER` int(11) NOT NULL,
  `ADRESSE` varchar(255) NOT NULL,
  `SURFACE_TOTALE` int(11) NOT NULL,
  `SURFACE_CHAMBRE` int(11) NOT NULL,
  `PRIX_LOCATION` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ID_QUARTIER` (`ID_QUARTIER`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `logement`
--

INSERT INTO `logement` (`ID`, `ID_QUARTIER`, `ADRESSE`, `SURFACE_TOTALE`, `SURFACE_CHAMBRE`, `PRIX_LOCATION`) VALUES
(1, 29, '20 rue Sainte Catherine', 30, 5, 507),
(2, 29, '5 cours Victor Hugo', 40, 7, 676),
(3, 127, '4 rue Nelson Mandela', 45, 10, 563),
(9, 30, '8 rue Le Reynard', 95, 10, 1786),
(18, 37, '20 rue Test', 40, 10, 520),
(19, 18, '20 rue de Bègles', 50, 7, 630),
(22, 19, '3 rue Test', 45, 6, 567),
(23, 6, 'Test', 100, 10, 1220),
(24, 13, '1 rue de Centujean Street', 35, 7, 445),
(25, 85, 'test le bouscat', 30, 5, 390),
(26, 32, '1 rue Bouron', 70, 10, 1092),
(28, 3, '20 rue test', 100, 20, 1190),
(29, 33, '20 rue AZERTT', 100, 10, 1380),
(30, 33, '20 rue AZERTT', 50, 5, 690),
(31, 33, '20 rue AZERTT', 50, 5, 690),
(32, 83, 'lkcndlckn', 111, 11, 1510),
(33, 83, 'lkcndlckn', 111, 11, 1510),
(55, 6, '22 rue Victor Louis', 120, 12, 1464),
(56, 28, '3 rue Eiffel', 120, 12, 1488),
(58, 37, '1 rue de Bouliac', 100, 10, 1300),
(59, 37, 'testserv', 80, 12, 1040),
(60, 28, '3 rue Jean', 90, 10, 1116),
(61, 31, '1 rue Pierre', 100, 12, 1640);

-- --------------------------------------------------------

--
-- Structure de la table `possede_service`
--

DROP TABLE IF EXISTS `possede_service`;
CREATE TABLE IF NOT EXISTS `possede_service` (
  `ID_LOGEMENT` int(11) NOT NULL,
  `ID_SERVICE` int(11) NOT NULL,
  KEY `FK_ID_SERVICE` (`ID_SERVICE`),
  KEY `FK_ID_LOGEMENT` (`ID_LOGEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `possede_service`
--

INSERT INTO `possede_service` (`ID_LOGEMENT`, `ID_SERVICE`) VALUES
(1, 1),
(1, 3),
(2, 4),
(2, 2),
(3, 1),
(3, 5),
(3, 3),
(9, 1),
(9, 3),
(9, 5),
(18, 4),
(18, 5),
(19, 3),
(19, 5),
(24, 5),
(25, 4),
(25, 5),
(28, 3),
(28, 5),
(29, 5),
(30, 2),
(30, 4),
(30, 5),
(31, 2),
(31, 4),
(31, 5),
(33, 4),
(55, 5),
(56, 2),
(61, 3),
(61, 4),
(61, 5);

-- --------------------------------------------------------

--
-- Structure de la table `quartier`
--

DROP TABLE IF EXISTS `quartier`;
CREATE TABLE IF NOT EXISTS `quartier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_VILLE` int(11) NOT NULL,
  `INTITULE` varchar(255) NOT NULL,
  `PRIX_M2` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ID_VILLE` (`ID_VILLE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `quartier`
--

INSERT INTO `quartier` (`ID`, `ID_VILLE`, `INTITULE`, `PRIX_M2`) VALUES
(1, 1, 'Chemin de la Vie', 11.4),
(2, 1, 'Sabaeges', 11.3),
(3, 1, 'Parabelle', 11.9),
(4, 1, 'Lagrave - Le Bourg', 11.8),
(5, 2, 'Ambes', 11),
(6, 3, 'Nord', 12.2),
(7, 3, 'Sud', 11.9),
(8, 4, 'Ouest - Quais', 12),
(9, 4, 'Nord', 12),
(10, 4, 'Centre', 12),
(11, 4, 'Sud', 12),
(12, 5, 'La Castagne - La Ferrade', 12.1),
(13, 5, 'Centujean', 12.7),
(14, 5, 'La Preche', 12.1),
(15, 5, 'Sembat', 11.9),
(16, 5, 'La Raze', 12.6),
(17, 5, 'Argous', 12.6),
(18, 5, 'Mairie Bourg', 12.6),
(19, 5, 'Dorat Verduc', 12.6),
(20, 5, 'Birambits', 12.5),
(21, 5, 'Zone d\'Activité', 12.6),
(22, 6, 'Marnieres', 12.4),
(23, 6, 'Caychac Bourg', 12.4),
(24, 6, 'Saturne', 12.4),
(25, 6, 'Centre Ville', 13),
(26, 6, 'Majolan', 12.4),
(27, 6, 'Zone Industrielle', 12.4),
(28, 6, 'Bords de Garonne', 12.4),
(29, 7, 'Hôtel de ville - Quinconces - Saint Seurin - Fondaudège', 16.9),
(30, 7, 'Capucins - Saint Michel - Nansouty - Saint Gènes', 18.8),
(31, 7, 'Saint Jean - Belcier - Carle Vernet - Albert 1er', 16.4),
(32, 7, 'Saint Bruno - Saint Augustin', 15.6),
(33, 7, 'La Bastide', 13.8),
(34, 7, 'Grand Parc - Chartrons - Paul Doumer', 15.2),
(35, 7, 'Le Lac - Bacalan', 13.8),
(36, 7, 'Caudéran - Barrière Judaïque', 13.6),
(37, 8, 'Bouliac', 13),
(38, 10, 'Villabois - La Hutte', 11.9),
(39, 10, 'Cimetière Nord - Petit Bruges', 12.1),
(40, 10, 'La Marianne - Sainte Germaine', 11.9),
(41, 10, 'Tour de Gassies - Treulon', 13.1),
(42, 10, 'Centre Ville', 13.2),
(43, 11, 'Nord', 12.5),
(44, 11, 'Centre', 11.9),
(45, 11, 'Sud', 11.9),
(46, 12, 'Bas Cenon - Cours Verdun - Testaud', 14),
(47, 12, 'Bas Cenon - Cours Victor Hugo - Gambetta', 12.5),
(48, 12, 'Cavailles - Camparian', 11.5),
(49, 12, 'Palmer', 12.4),
(50, 12, 'Gravières - Gravette - Beaulieu - Lagrue - Plaisance', 12.9),
(51, 12, 'Saraillere - Ronceval - Seiglere', 10.9),
(52, 12, 'Grand Pavois - Cimetière Saint Paul', 12.5),
(53, 12, 'Marègue', 12.5),
(54, 13, 'Bourg', 12.6),
(55, 13, 'Cantinolle', 11.3),
(56, 13, 'Vigean', 12.4),
(57, 13, 'Derby', 12.2),
(58, 13, 'Migron', 12.4),
(59, 13, 'Grand Louis', 12.4),
(60, 13, 'La Lesque', 13.4),
(61, 14, 'La Marègue Etendu', 11.8),
(62, 14, 'Coteaux', 12),
(63, 14, 'Rebedech', 11.8),
(64, 14, 'Plateaux', 11.8),
(65, 14, 'Gambetta', 11.8),
(66, 14, 'Gravette', 11.8),
(67, 14, 'La Souys', 11.8),
(68, 15, 'Naudet', 18.7),
(69, 15, 'Brannes', 20),
(70, 15, 'Le Brandier', 12.5),
(71, 15, 'Saint Gery', 11.5),
(72, 15, 'Moulineau', 13.1),
(73, 15, 'Beausoleil', 13.3),
(74, 15, 'Loustalot', 13.3),
(75, 15, 'Malartic', 13.3),
(76, 15, 'Zone d\'Activité Bersol', 13.3),
(77, 15, 'Ornon', 13.3),
(78, 9, 'Jean Jaurès - Ravezie', 12.8),
(79, 9, 'Marceau - Tivoli', 12),
(80, 9, 'Barrière du Médoc', 12.8),
(81, 9, 'Jean Moulin', 12.8),
(82, 9, 'Centre - Ermitage', 12.5),
(83, 9, 'La Garenne - Baudin', 13.6),
(84, 9, 'Ausone - Les Ecus', 12.8),
(85, 9, 'Cheneraie - Lavigne', 13),
(86, 9, 'Hippodrome - Lafon - Feline', 12.8),
(87, 16, 'Gasquet', 12.6),
(88, 16, 'Miotte', 11.5),
(89, 16, 'Bechade', 12.5),
(90, 16, 'Baraillot', 12.6),
(91, 26, 'Sud Est', 10.9),
(92, 26, 'Centre Bourg', 11.8),
(93, 26, 'Nord des Jalles', 11.8),
(94, 17, 'Cité Carriet', 12.6),
(95, 17, 'Cité Carriet - La Croix Rouge', 12.6),
(96, 17, 'Le Bourg - Le Vieux Bourg', 12.3),
(97, 17, 'Genicart', 11.2),
(98, 17, 'Lissandre', 12.4),
(99, 17, 'Zup', 12.6),
(100, 17, 'Zone Industrielle Quatre Pavillons - La Gardette', 12.3),
(101, 17, 'Résidences des Hauts de Lormont', 12.6),
(102, 18, 'Nord', 13.1),
(103, 18, 'Sud', 12.8),
(104, 19, 'Arlac', 16.5),
(105, 19, 'Le Burck', 13),
(106, 19, 'Les Eyquems', 13.3),
(107, 19, 'La Glacière - Labatut', 12.1),
(108, 19, 'La Glacière - Bourdillot', 13.8),
(109, 19, 'La Glacière - Mondésir', 13),
(110, 19, 'Bouranville - Le Jard', 13),
(111, 19, 'Centre Ville', 12.7),
(112, 19, 'Capeyron', 11.5),
(113, 19, 'Capeyron - La Forêt', 11.7),
(114, 19, 'Capeyron - Les Pins', 13),
(115, 19, 'Centre Ville - Piquey', 13.4),
(116, 19, 'Chemin Long - Garies', 13.5),
(117, 19, 'Beutre', 13.3),
(118, 19, 'Beaudésert', 13.3),
(119, 20, 'Ouest', 12.6),
(120, 20, 'Centre', 10.9),
(121, 20, 'Est', 11.6),
(122, 21, 'Verthamon - Haut Brion', 13),
(123, 21, 'Les Echoppes', 13),
(124, 21, 'Brivazac - Cadau', 13),
(125, 21, 'Brivazac', 18.8),
(126, 21, 'Noës', 13),
(127, 21, 'Bourg', 12.5),
(128, 21, 'Sardine - Chiquet', 12.5),
(129, 21, 'Compostelle - La Paillère', 13),
(130, 21, 'Saige Nord', 13),
(131, 21, 'Saige Sud', 16.3),
(132, 21, 'L\'Alouette - Saige', 13),
(133, 21, 'Le Monteil - Madran', 13),
(134, 21, 'Le Monteil - Paul Montagne', 12.8),
(135, 21, 'CCLAPS - Camponac', 10.7),
(136, 21, 'La Châtaignerie', 13),
(137, 21, 'L\'Alouette - Hôpital Xavier Arnozan', 13),
(138, 21, '3M', 13),
(139, 21, 'L\'Alouette - Cazalet', 13),
(140, 21, 'France - L\'Alouette - Jean Bart', 11.9),
(141, 21, 'Cap de Bos', 13),
(142, 21, 'Magonty', 13),
(143, 21, 'Toctoucau', 13),
(144, 22, 'Saint Aubin de Médoc', 13.2),
(145, 23, 'Saint Louis de Montferrand', 13.2),
(146, 24, 'Magudas', 12.5),
(147, 24, 'Corbiac', 12.5),
(148, 24, 'Gajac', 12.5),
(149, 24, 'Zone d\'Activité (SNPE)', 12.5),
(150, 24, 'Centre Ville', 12),
(151, 24, 'Caupian - Sans Soucis', 14.2),
(152, 24, 'Zone d\'Activité (Caepe - Aerospaciale)', 12.5),
(153, 24, 'Issac - Cerillan', 11.7),
(154, 24, 'Hastignan', 12.5),
(155, 24, 'Hastignan - Picot', 12.5),
(156, 24, 'Zone route du Porge', 12.5),
(157, 25, 'Saint Vincent de Paul', 13.2),
(158, 27, 'Zola - La Taillade', 16),
(159, 27, 'Médoquine - Haut Brion', 16.4),
(160, 27, 'Saint Genès', 18.3),
(161, 27, 'Poste Mairie', 17.4),
(162, 27, 'Caudèrès', 18.6),
(163, 27, 'La Fauvette', 15.9),
(164, 27, 'Bijou - Thouars', 17.2),
(165, 27, 'Le Lycée', 15.6),
(166, 27, 'Peylanne', 13.9),
(167, 27, 'Plume la Boule - Pacaris', 13.4),
(168, 27, 'Plume la Boule - Raba', 16),
(169, 27, 'Château de Thouars', 10.7),
(170, 28, 'Croix de Leysotte - Chanteloiseau - Saint Bris', 18.8),
(171, 28, 'Saint Martin - Canteloup - Bourleaux', 12.1),
(172, 28, 'Bauge - Sarcignan - Versein', 12.1),
(173, 28, 'Résidence de la Hé - Château Baret - Soors', 12.6),
(174, 28, 'Chambery', 12.1),
(175, 28, 'Petit Chambery - Brown', 12.1),
(176, 28, 'La Monnaie - La Taille', 11.3),
(177, 28, 'Peyrehaut - Carbonnieux - Trigan', 12.1),
(178, 28, 'Clémenceau - Pontac - Sallegourde - Pont de Langon', 11.4),
(179, 28, 'Hourcade - Geneste - Le Bourg - Courrejean', 11.9);

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `INTITULE` varchar(255) NOT NULL,
  `FREQUENCE` int(11) NOT NULL,
  `REMISE` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `service`
--

INSERT INTO `service` (`ID`, `INTITULE`, `FREQUENCE`, `REMISE`) VALUES
(1, 'Faire les courses', 1, 15),
(2, 'Faire les courses', 4, 55),
(3, 'Jardinage', 1, 40),
(4, 'Babysitting', 1, 20),
(5, 'Babysitting', 5, 100);

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

DROP TABLE IF EXISTS `statut`;
CREATE TABLE IF NOT EXISTS `statut` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `INTITULE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`ID`, `INTITULE`) VALUES
(1, 'Etudiant'),
(2, 'Famille'),
(3, 'Senior');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `INTITULE` varchar(255) NOT NULL,
  `CODE_POSTAL` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`ID`, `INTITULE`, `CODE_POSTAL`) VALUES
(1, 'Ambarès-et-Lagrave', 33440),
(2, 'Ambès', 33810),
(3, 'Artigues-près-Bordeaux', 33370),
(4, 'Bassens', 33530),
(5, 'Bègles', 33130),
(6, 'Blanquefort', 33290),
(7, 'Bordeaux', 33000),
(8, 'Bouliac', 33270),
(9, 'Le Bouscat', 33110),
(10, 'Bruges', 33520),
(11, 'Carbon-Blanc', 33560),
(12, 'Cenon', 33150),
(13, 'Eysines', 33320),
(14, 'Floirac', 33270),
(15, 'Gradignan', 33170),
(16, 'Le Haillan', 33185),
(17, 'Lormont', 33310),
(18, 'Martignas-sur-Jalle', 33127),
(19, 'Mérignac', 33700),
(20, 'Parempuyre', 33290),
(21, 'Pessac', 33600),
(22, 'Saint-Aubin-de-Médoc', 33160),
(23, 'Saint-Louis-de-Montferrand', 33440),
(24, 'Saint-Médard-en-Jalles', 33160),
(25, 'Saint-Vincent-de-Paul', 33440),
(26, 'Le Taillan-Médoc', 33320),
(27, 'Talence', 33400),
(28, 'Villenave-d\'Ornon', 33140);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `FK_ID_STATUT` FOREIGN KEY (`ID_STATUT`) REFERENCES `statut` (`ID`);

--
-- Contraintes pour la table `est_bailleur`
--
ALTER TABLE `est_bailleur`
  ADD CONSTRAINT `FK_ID_CLIENT_B` FOREIGN KEY (`ID_CLIENT`) REFERENCES `client` (`ID`),
  ADD CONSTRAINT `FK_ID_LOGEMENT_B` FOREIGN KEY (`ID_LOGEMENT`) REFERENCES `logement` (`ID`);

--
-- Contraintes pour la table `est_locataire`
--
ALTER TABLE `est_locataire`
  ADD CONSTRAINT `FK_ID_CLIENT_L` FOREIGN KEY (`ID_CLIENT`) REFERENCES `client` (`ID`),
  ADD CONSTRAINT `FK_ID_LOGEMENT_L` FOREIGN KEY (`ID_LOGEMENT`) REFERENCES `logement` (`ID`);

--
-- Contraintes pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD CONSTRAINT `FK_ID_CLIENT_F` FOREIGN KEY (`ID_CLIENT`) REFERENCES `client` (`ID`),
  ADD CONSTRAINT `FK_ID_LOGEMENT_F` FOREIGN KEY (`ID_LOGEMENT`) REFERENCES `logement` (`ID`);

--
-- Contraintes pour la table `logement`
--
ALTER TABLE `logement`
  ADD CONSTRAINT `FK_ID_QUARTIER` FOREIGN KEY (`ID_QUARTIER`) REFERENCES `quartier` (`ID`);

--
-- Contraintes pour la table `possede_service`
--
ALTER TABLE `possede_service`
  ADD CONSTRAINT `FK_ID_LOGEMENT` FOREIGN KEY (`ID_LOGEMENT`) REFERENCES `logement` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ID_SERVICE` FOREIGN KEY (`ID_SERVICE`) REFERENCES `service` (`ID`);

--
-- Contraintes pour la table `quartier`
--
ALTER TABLE `quartier`
  ADD CONSTRAINT `FK_ID_VILLE_Q` FOREIGN KEY (`ID_VILLE`) REFERENCES `ville` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
